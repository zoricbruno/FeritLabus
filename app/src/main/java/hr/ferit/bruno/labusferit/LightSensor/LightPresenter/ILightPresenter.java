package hr.ferit.bruno.labusferit.LightSensor.LightPresenter;

import hr.ferit.bruno.labusferit.Common.ISensorChangeListener;

/**
 * Created by Zoric on 21.12.2017..
 */

public interface ILightPresenter extends ISensorChangeListener {
    public void startTracking();
    public void stopTracking();
}
