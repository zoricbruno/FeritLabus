package hr.ferit.bruno.labusferit.AccelerationSensor.Model;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import hr.ferit.bruno.labusferit.Common.ISensor;
import hr.ferit.bruno.labusferit.Common.ISensorChangeListener;
import hr.ferit.bruno.labusferit.FeritLabusApplication;

/**
 * Created by Stray on 23.12.2017..
 */

public class AndroidAccelerationSensor implements ISensor, SensorEventListener {

	ISensorChangeListener mAccelerationChangeListener;

	private static final long MIN_UPDATE_TIME = 500;

	Sensor mAccelerationSensor;
	SensorManager mSensorManager;
	long mLastTimestamp = -1;

	public AndroidAccelerationSensor(ISensorChangeListener listener) {

		Context context = FeritLabusApplication.getInstance();
		mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		mAccelerationSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		if(mAccelerationSensor == null) {
			throw new RuntimeException("Sensor not available.");
		}
		mAccelerationChangeListener = listener;
	}


	@Override
	public void startTracking() {
		mSensorManager.registerListener(this, mAccelerationSensor, SensorManager.SENSOR_DELAY_NORMAL);
	}

	@Override
	public void stopTracking() {
		mSensorManager.unregisterListener(this);
	}

	@Override
	public void onSensorChanged(SensorEvent sensorEvent) {
		if(System.currentTimeMillis() - mLastTimestamp > MIN_UPDATE_TIME){
			float[] values = sensorEvent.values;
			this.mAccelerationChangeListener.onSensorMeasurmentChanged(values);
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int i) {

	}
}
