package hr.ferit.bruno.labusferit.LightSensor.View;

/**
 * Created by Zoric on 21.12.2017..
 */

public interface ILightView {
    void displayLightIntensity(float intensity);
    void displayLightNits(double nits);
    void displayLightLux(double lux);
    void displayMessage(String message);
}
