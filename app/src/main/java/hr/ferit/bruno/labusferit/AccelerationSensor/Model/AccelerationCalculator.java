package hr.ferit.bruno.labusferit.AccelerationSensor.Model;

/**
 * Created by Stray on 23.12.2017..
 */

public class AccelerationCalculator {
	public static double accelerationFromComponents(float[] components){
		double res = 0.0;
		for(float f : components){
			res += f*f;
		}
		res = Math.sqrt(res);
		return res;
	}
}
