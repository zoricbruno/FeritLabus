package hr.ferit.bruno.labusferit.MagnetSensor.Model;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import hr.ferit.bruno.labusferit.Common.ISensor;
import hr.ferit.bruno.labusferit.Common.ISensorChangeListener;
import hr.ferit.bruno.labusferit.FeritLabusApplication;

/**
 * Created by Zoric on 27.12.2017..
 */

public class AndroidMagneticSensor implements ISensor, SensorEventListener {

    private static final long MIN_UPDATE_TIME = 500;

    private Sensor mMagneticFieldSensor;
    private SensorManager mSensorManager;
    private ISensorChangeListener mMagnetChangeListener;
    private long mLastTimestamp = -1;

    public AndroidMagneticSensor(ISensorChangeListener listener) {

        Context context = FeritLabusApplication.getInstance();
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mMagneticFieldSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        if(mMagneticFieldSensor == null) {
            throw new RuntimeException("Sensor not available.");
        }

        mMagnetChangeListener = listener;
    }

    @Override
    public void startTracking() {
        mSensorManager.registerListener(this, mMagneticFieldSensor, SensorManager.SENSOR_DELAY_NORMAL);

    }

    @Override
    public void stopTracking() {
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if(System.currentTimeMillis() - mLastTimestamp > MIN_UPDATE_TIME){
            float[] values = sensorEvent.values;
            this.mMagnetChangeListener.onSensorMeasurmentChanged(values);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
