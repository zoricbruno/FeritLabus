package hr.ferit.bruno.labusferit.MagnetSensor.Model;

import com.github.mikephil.charting.charts.ScatterChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.ScatterData;
import com.github.mikephil.charting.data.ScatterDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import hr.ferit.bruno.labusferit.R;

/**
 * Created by Stray on 27.12.2017..
 */

public class MagnetHistory {

	private int mMaxData = 20;

	// Track all of the axis:
	private final static String LABEL_X = "xAxis";
	private final static String LABEL_Y = "yAxis";
	private final static String LABEL_Z = "zAxis";

	ScatterData mData;

	public MagnetHistory() {
		mData = new ScatterData();

		ScatterDataSet dataSetX = new ScatterDataSet(new ArrayList<Entry>(), LABEL_X);
		dataSetX.setScatterShape(ScatterChart.ScatterShape.TRIANGLE);
		dataSetX.setColor(ColorTemplate.COLORFUL_COLORS[0]);

		ScatterDataSet dataSetY = new ScatterDataSet(new ArrayList<Entry>(), LABEL_Y);
		dataSetY.setScatterShape(ScatterChart.ScatterShape.CIRCLE);
		dataSetY.setColor(ColorTemplate.COLORFUL_COLORS[3]);

		ScatterDataSet dataSetZ = new ScatterDataSet(new ArrayList<Entry>(), LABEL_Z);
		dataSetZ.setScatterShape(ScatterChart.ScatterShape.SQUARE);
		dataSetZ.setColor(ColorTemplate.COLORFUL_COLORS[2]);

		mData.addDataSet(dataSetX);
		mData.addDataSet(dataSetY);
		mData.addDataSet(dataSetZ);
	}

	public void addValues(float[] values){
		for (int i=0;i<mData.getDataSetCount();i++) {
			this.addEntry(values[i], i);
		}
	}

	private void addEntry(float value, int i) {

		ScatterDataSet dataSet = (ScatterDataSet) mData.getDataSetByIndex(i);
		float index = dataSet.getEntryCount();
		Entry newEntry = new Entry(index, value);
		if(index > mMaxData)
		{
			dataSet.removeFirst();
			for(Entry entry : dataSet.getValues()){
				entry.setX(entry.getX() -1);
			}
		}
		dataSet.addEntry(newEntry);
		dataSet.notifyDataSetChanged();
		mData.notifyDataChanged();

//		Entry newEntry = new Entry(mMaxData/3, value);
//		mData.addEntry(newEntry, i);
//		mMaxData++;
	}

	public ScatterData getData(){
		return mData;
	}
}
