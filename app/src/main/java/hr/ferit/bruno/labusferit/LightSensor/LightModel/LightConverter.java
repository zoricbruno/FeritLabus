package hr.ferit.bruno.labusferit.LightSensor.LightModel;

/**
 * Created by Zoric on 22.12.2017..
 */

public class LightConverter {
    public static double toNits(double lux){
        return lux / Math.PI;
    }

    public static double toLux(double nits){
        return nits * Math.PI;
    }
}
