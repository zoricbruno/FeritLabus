package hr.ferit.bruno.labusferit.MagnetSensor.Presenter;

import hr.ferit.bruno.labusferit.Common.ISensorChangeListener;

/**
 * Created by Zoric on 27.12.2017..
 */

public interface IMagnetPresenter extends ISensorChangeListener{
    public void startTracking();
    public void stopTracking();
}
