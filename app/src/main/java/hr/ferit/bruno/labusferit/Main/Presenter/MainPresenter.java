package hr.ferit.bruno.labusferit.Main.Presenter;

import hr.ferit.bruno.labusferit.Main.View.IMainView;

/**
 * Created by Zoric on 21.12.2017..
 */

public class MainPresenter implements IMainPresenter {

    IMainView view;

    public MainPresenter(IMainView view) {
        this.view = view;
    }

    @Override
    public void onGoToLightClicked() {
        view.goToLightSensor();
    }

    @Override
    public void onGoToAccelerationClicked() {
        view.goToAccelerationSensor();
    }

    @Override
    public void goToMagneticClicked() {
        view.goToMagneticSensor();
    }
}
