package hr.ferit.bruno.labusferit.MagnetSensor.View;

import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.LineData;

/**
 * Created by Zoric on 27.12.2017..
 */

public interface IMagnetView {

    // Display magnetic force by axis:
    void displayXAxis(float value);
    void displayYAxis(float value);
    void displayZAxis(float value);
    void displayComponents(ChartData values);

    // Display total magnetic force:
    void displayTotalGraphic(float value);
    void displayTotalValue(float value);

    void displayDialog(String message);
}
