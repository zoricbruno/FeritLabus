package hr.ferit.bruno.labusferit.MagnetSensor.View;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.github.mikephil.charting.charts.ScatterChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.ScatterData;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.ferit.bruno.labusferit.FeritLabusApplication;
import hr.ferit.bruno.labusferit.MagnetSensor.Presenter.IMagnetPresenter;
import hr.ferit.bruno.labusferit.MagnetSensor.Presenter.MagnetPresenter;
import hr.ferit.bruno.labusferit.R;
import hr.ferit.bruno.labusferit.Utilities.Constants;
import hr.ferit.bruno.labusferit.Utilities.Formatting;

public class MagnetActivity extends AppCompatActivity implements IMagnetView{

    IMagnetPresenter mPresenter;

    @BindView(R.id.tvMagneticX) TextView tvMagneticXAxis;
    @BindView(R.id.tvMagneticY) TextView tvMagneticYAxis;
    @BindView(R.id.tvMagneticZ) TextView tvMagneticZAxis;
    @BindView(R.id.tvTotalMagneticField) TextView tvTotalMagneticField;
    @BindView(R.id.rpbTotalMagneticField) RoundCornerProgressBar rpbTotalMagneticField;
    @BindView(R.id.spMagneticFieldComponents) ScatterChart spMagneticFieldComponents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_magnet);
        ButterKnife.bind(this);

        this.mPresenter = new MagnetPresenter(this);
        this.setUpChart();
    }

	private void setUpChart() {
		Legend legend = spMagneticFieldComponents.getLegend();
		legend.setEnabled(true);
		legend.setTextColor(Color.WHITE);
	}

	@Override
    protected void onResume() {
        super.onResume();
        mPresenter.startTracking();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.stopTracking();
    }

    @Override
    public void displayXAxis(float value) {
        tvMagneticXAxis.setText(Formatting.doubleToTwoDecimalPlaces(value));
    }

    @Override
    public void displayYAxis(float value) {
        tvMagneticYAxis.setText(Formatting.doubleToTwoDecimalPlaces(value));
    }

    @Override
    public void displayZAxis(float value) {
        tvMagneticZAxis.setText(Formatting.doubleToTwoDecimalPlaces(value));
    }

	@Override
	public void displayComponents(ChartData values) {
     	spMagneticFieldComponents.setData((ScatterData) values);
     	spMagneticFieldComponents.notifyDataSetChanged();
    	spMagneticFieldComponents.invalidate();
	}

	@Override
    public void displayTotalGraphic(float value) {
        rpbTotalMagneticField.setProgress(value);
    }

    @Override
    public void displayTotalValue(float value) {
        tvTotalMagneticField.setText(Formatting.doubleToTwoDecimalPlaces(value));
    }

    @Override
    public void displayDialog(String message) {
        Toast.makeText(FeritLabusApplication.getInstance(),message, Constants.SHORT).show();
    }
}
