package hr.ferit.bruno.labusferit.Common;

/**
 * Created by Zoric on 22.12.2017..
 */

public interface ISensorChangeListener {
    public void onSensorMeasurmentChanged(float[] values);
}
