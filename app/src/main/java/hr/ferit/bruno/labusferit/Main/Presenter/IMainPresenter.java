package hr.ferit.bruno.labusferit.Main.Presenter;

/**
 * Created by Zoric on 21.12.2017..
 */

public interface IMainPresenter {

    void onGoToLightClicked();

    void onGoToAccelerationClicked();

    void goToMagneticClicked();
}
