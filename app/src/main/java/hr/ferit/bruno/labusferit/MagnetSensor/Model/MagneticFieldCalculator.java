package hr.ferit.bruno.labusferit.MagnetSensor.Model;

/**
 * Created by Zoric on 27.12.2017..
 */

public class MagneticFieldCalculator {

    public static float calculateMagneticField(float[] values){
        float sum = 0.0F;
        for (float value : values){
            sum += value * value;
        }
        return (float)Math.sqrt(sum);
    }
}
