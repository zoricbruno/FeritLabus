package hr.ferit.bruno.labusferit.Common;

/**
 * Created by Zoric on 22.12.2017..
 */

public interface ISensor {
    public void startTracking();
    public void stopTracking();
}
