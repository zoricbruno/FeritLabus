package hr.ferit.bruno.labusferit;

import android.app.Application;

/**
 * Created by Zoric on 21.12.2017..
 */

public class FeritLabusApplication extends Application {

    private static FeritLabusApplication instance;

    public static FeritLabusApplication getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
