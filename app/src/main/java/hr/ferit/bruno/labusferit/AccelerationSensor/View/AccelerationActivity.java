package hr.ferit.bruno.labusferit.AccelerationSensor.View;

import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.ferit.bruno.labusferit.AccelerationSensor.Presenter.AccelerationPresenter;
import hr.ferit.bruno.labusferit.AccelerationSensor.Presenter.IAccelerationPresenter;
import hr.ferit.bruno.labusferit.FeritLabusApplication;
import hr.ferit.bruno.labusferit.R;
import hr.ferit.bruno.labusferit.Utilities.Constants;
import hr.ferit.bruno.labusferit.Utilities.Formatting;

public class AccelerationActivity extends AppCompatActivity implements IAccelerationView{

	@BindView(R.id.rlRootView) RelativeLayout rlRootView;
	@BindView(R.id.tvAcceleration) TextView tvAcceleration;
	@BindView(R.id.tvAccelerationX) TextView tvAccelerationX;
	@BindView(R.id.tvAccelerationY) TextView tvAccelerationY;
	@BindView(R.id.tvAccelerationZ) TextView tvAccelerationZ;
	@BindView(R.id.rpbAccelerationX) RoundCornerProgressBar rpbAccelerationX;
	@BindView(R.id.rpbAccelerationY) RoundCornerProgressBar rpbAccelerationY;
	@BindView(R.id.rpbAccelerationZ) RoundCornerProgressBar rpbAccelerationZ;

	IAccelerationPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acceleration);
		ButterKnife.bind(this);

		mPresenter = new AccelerationPresenter(this);
    }

	@Override
	protected void onResume() {
		super.onResume();
		mPresenter.startTracking();
	}

	@Override
	protected void onPause() {
		super.onPause();
		mPresenter.stopTracking();
	}

	@Override
    public void excessAcceleration() {
		rlRootView.setBackgroundResource(R.color.colorRed);
    }

    @Override
    public void normalAcceleration() {
    	rlRootView.setBackgroundResource(R.color.colorTeal);
    }

	@Override
	public void displayXAcc(double xAcc) {
    	tvAccelerationX.setText(Formatting.doubleToTwoDecimalPlaces(xAcc));
    	rpbAccelerationX.setProgress((float)xAcc);
	}

	@Override
	public void displayYAcc(double yAcc) {
		tvAccelerationY.setText(Formatting.doubleToTwoDecimalPlaces(yAcc));
		rpbAccelerationY.setProgress((float)yAcc);
	}

	@Override
	public void displayZAcc(double zAcc) {
		tvAccelerationZ.setText(Formatting.doubleToTwoDecimalPlaces(zAcc));
		rpbAccelerationZ.setProgress((float)zAcc);
	}

	@Override
	public void displayTotal(double totalAcc) {
		tvAcceleration.setText(Formatting.doubleToTwoDecimalPlaces(totalAcc));
	}

	@Override
	public void playSound() {
    	Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    	Ringtone ringtone = RingtoneManager.getRingtone(this, notification);
    	ringtone.play();
	}

	@Override
    public void displayDialog(String message) {
        Toast.makeText(FeritLabusApplication.getInstance(), message, Constants.SHORT).show();
    }
}
