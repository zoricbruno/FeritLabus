package hr.ferit.bruno.labusferit.LightSensor.LightPresenter;

import hr.ferit.bruno.labusferit.Common.ISensor;
import hr.ferit.bruno.labusferit.Common.ISensorChangeListener;
import hr.ferit.bruno.labusferit.LightSensor.LightModel.AndroidLightSensor;
import hr.ferit.bruno.labusferit.LightSensor.LightModel.LightConverter;
import hr.ferit.bruno.labusferit.LightSensor.View.ILightView;

/**
 * Created by Zoric on 21.12.2017..
 */

public class LightPresenter implements ILightPresenter {

    ILightView mLightView;
    ISensor mLightSensor;

    public LightPresenter(ILightView view) {
        mLightView = view;
        try{
            mLightSensor = new AndroidLightSensor(this);
        }
        catch (RuntimeException exception){
            mLightView.displayMessage(exception.getMessage());
        }
    }

    @Override
    public void startTracking() {
        if(mLightSensor != null){
            mLightSensor.startTracking();
        }
    }

    @Override
    public void stopTracking() {
        if(mLightSensor != null){
            mLightSensor.stopTracking();
        }
    }

    @Override
    public void onSensorMeasurmentChanged(float[] values) {
        mLightView.displayLightLux(values[0]);
        mLightView.displayLightNits(LightConverter.toNits(values[0]));
        mLightView.displayLightIntensity((float)values[0]);
    }
}
