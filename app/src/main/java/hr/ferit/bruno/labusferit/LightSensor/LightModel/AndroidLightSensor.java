package hr.ferit.bruno.labusferit.LightSensor.LightModel;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import hr.ferit.bruno.labusferit.Common.ISensor;
import hr.ferit.bruno.labusferit.Common.ISensorChangeListener;
import hr.ferit.bruno.labusferit.FeritLabusApplication;

/**
 * Created by Zoric on 21.12.2017..
 */

public class AndroidLightSensor implements ISensor, SensorEventListener {

    private static final long MIN_UPDATE_TIME = 500;

    ISensorChangeListener mLightChangeListener;
    Sensor mLightSensor;
    SensorManager mSensorManager;
    long mLastTimestamp = -1;

    public AndroidLightSensor(ISensorChangeListener listener) {
        Context context = FeritLabusApplication.getInstance();
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mLightSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if(mLightSensor == null) {
            throw new RuntimeException("Sensor not available.");
        }
        this.mLightChangeListener = listener;
    }

    @Override
    public void startTracking() {
        mSensorManager.registerListener(this, mLightSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void stopTracking() {
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if(System.currentTimeMillis() - mLastTimestamp > MIN_UPDATE_TIME){
            float[] lux = new float[]{ sensorEvent.values[0] };
            this.mLightChangeListener.onSensorMeasurmentChanged(lux);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) { }
}
