package hr.ferit.bruno.labusferit.MagnetSensor.Presenter;

import android.util.Log;

import hr.ferit.bruno.labusferit.Common.ISensor;
import hr.ferit.bruno.labusferit.MagnetSensor.Model.AndroidMagneticSensor;
import hr.ferit.bruno.labusferit.MagnetSensor.Model.MagnetHistory;
import hr.ferit.bruno.labusferit.MagnetSensor.Model.MagneticFieldCalculator;
import hr.ferit.bruno.labusferit.MagnetSensor.View.IMagnetView;
import hr.ferit.bruno.labusferit.Utilities.Constants;

/**
 * Created by Zoric on 27.12.2017..
 */

public class MagnetPresenter implements IMagnetPresenter {

    IMagnetView mView;
    ISensor mMagnetSensor;
    MagnetHistory mHistory;

    public MagnetPresenter(IMagnetView view) {
        mView = view;
        mHistory = new MagnetHistory();
        try{
            mMagnetSensor = new AndroidMagneticSensor(this);
        }
        catch (RuntimeException exception){
            mView.displayDialog(exception.getMessage());
        }
    }

    @Override
    public void onSensorMeasurmentChanged(float[] values) {
        mView.displayXAxis(values[0]);
        mView.displayYAxis(values[1]);
        mView.displayZAxis(values[2]);

        float totalMagneticField = MagneticFieldCalculator.calculateMagneticField((values));
        mView.displayTotalValue(totalMagneticField);
        mView.displayTotalGraphic(totalMagneticField);

        mHistory.addValues(values);
        mView.displayComponents(mHistory.getData());
    }

    @Override
    public void startTracking() {
        if(mMagnetSensor != null){
            mMagnetSensor.startTracking();
        }
    }

    @Override
    public void stopTracking() {
        if(mMagnetSensor != null){
            mMagnetSensor.stopTracking();
        }
    }
}
