package hr.ferit.bruno.labusferit.Main.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.bruno.labusferit.AccelerationSensor.View.AccelerationActivity;
import hr.ferit.bruno.labusferit.FeritLabusApplication;
import hr.ferit.bruno.labusferit.LightSensor.View.LightViewActivity;
import hr.ferit.bruno.labusferit.MagnetSensor.View.MagnetActivity;
import hr.ferit.bruno.labusferit.Main.Presenter.IMainPresenter;
import hr.ferit.bruno.labusferit.Main.Presenter.MainPresenter;
import hr.ferit.bruno.labusferit.R;

public class MainActivity extends AppCompatActivity implements IMainView {

    IMainPresenter presenter;

    @BindView(R.id.bGoToLightTracking) LinearLayout bGoToLightTracking;
    @BindView(R.id.bGoToMagnetometerTracking) LinearLayout bGoToMagnetometerTracking;
    @BindView(R.id.bGoToAccelerationTracking) LinearLayout bGoToAccelerationTracking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter = new MainPresenter(this);
    }

    @OnClick({R.id.bGoToMagnetometerTracking, R.id.bGoToAccelerationTracking, R.id.bGoToLightTracking})
    public void onClick(LinearLayout button){
        switch (button.getId()){
            case R.id.bGoToLightTracking:
                presenter.onGoToLightClicked();
                break;
            case R.id.bGoToAccelerationTracking:
                presenter.onGoToAccelerationClicked();
                break;
            case R.id.bGoToMagnetometerTracking:
                presenter.goToMagneticClicked();
                break;
        }
    }

    @Override
    public void goToLightSensor() {
        Intent intent = new Intent(FeritLabusApplication.getInstance(), LightViewActivity.class);
        startActivity(intent);
    }

    @Override
    public void goToAccelerationSensor() {
        Intent intent = new Intent(FeritLabusApplication.getInstance(), AccelerationActivity.class);
        startActivity(intent);
    }

    @Override
    public void goToMagneticSensor() {
        Intent intent = new Intent(FeritLabusApplication.getInstance(), MagnetActivity.class);
        startActivity(intent);
    }
}
