package hr.ferit.bruno.labusferit.Utilities;

import android.widget.Toast;

/**
 * Created by Zoric on 21.12.2017..
 */

public class Constants {
    public static final String TAG_DEBUG = "Ferit_Labus";
    public static final String NOT_IMPLEMENTED = "Not implemented";

    public static final int SHORT = Toast.LENGTH_SHORT;
}
