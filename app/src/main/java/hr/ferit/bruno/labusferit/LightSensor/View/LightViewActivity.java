package hr.ferit.bruno.labusferit.LightSensor.View;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.github.anastr.speedviewlib.SpeedView;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.ferit.bruno.labusferit.LightSensor.LightPresenter.ILightPresenter;
import hr.ferit.bruno.labusferit.LightSensor.LightPresenter.LightPresenter;
import hr.ferit.bruno.labusferit.R;
import hr.ferit.bruno.labusferit.Utilities.Constants;
import hr.ferit.bruno.labusferit.Utilities.Formatting;

public class LightViewActivity extends AppCompatActivity implements  ILightView{

    ILightPresenter mLightPresenter;

    @BindView(R.id.tvLightLuxValue) TextView tvLightLuxValue;
    @BindView(R.id.tvLightNitsValue) TextView tvLightNitsValue;
    @BindView(R.id.svLuxometer) SpeedView svLuxometer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_light);
        ButterKnife.bind(this);

        mLightPresenter = new LightPresenter(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLightPresenter.startTracking();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLightPresenter.stopTracking();
    }

    @Override
    public void displayLightIntensity(float intensity) {
        svLuxometer.speedTo(intensity);
    }

    @Override
    public void displayLightNits(double nits) {
        String lightInNits = Formatting.doubleToTwoDecimalPlaces(nits);
        tvLightNitsValue.setText(lightInNits);
    }

    @Override
    public void displayLightLux(double lux) {
        String lightInLux = Formatting.doubleToTwoDecimalPlaces(lux);
        tvLightLuxValue.setText(lightInLux);
    }

    @Override
    public void displayMessage(String message) {
        Toast.makeText(this, message, Constants.SHORT).show();
    }
}
